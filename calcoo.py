#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Calculadora():

    def __init__(self, op1, op2):
        self.op1 = op1
        self.op2 = op2

    def plus(self):
        return self.op1 + self.op2

    def minus(self):
        return self.op1 - self.op2


import sys

if __name__ == "__main__":

    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = Calculadora(op1, op2)

    if sys.argv[2] == "suma":
        print(objeto.plus())
    elif sys.argv[2] == "resta":
        print(objeto.minus())
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
